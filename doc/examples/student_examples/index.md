# Weitere Visualisierungsbeispiele

In einem Studierenden-Projekt an der Zürcher Hochschule der Künste entstanden vor einiger Zeit folgende vergleichende Visualisierungen. In die Richtung wird das Projekt VisuV experimentieren und die Aussage jeweils vereinfachen und kondensieren.

![Vergleich Cars and Roads von Patrick Müller](cars_and_roads_patrick_mueller.jpg)
Cars and roads von Patrick Müller

![Vergleich CO2 Emissionen von Marc Schneider](co2_emissions_marc_schneider.jpg)
Vergleich CO2 Emissionen von Marc Schneider

![Vergleich Internetzugang von Daniel Mischler](internet_access_daniel_mischler.jpg)
Vergleich Internetzugang von Daniel Mischler

![Vergleich Sauberes Wasser von Nils Solanki](water_sanitation_nils_solanki.jpg)
Vergleich Sauberes Wasser von Nils Solanki
