# VisuV

VisuV - Visueller Ländervergleich für mehr Kontext

Beispiel einer Visualisierung für den Themenbereich "Geographie"

![Geographie Vergleich Ägypten Deutschland](/doc/examples/sketches/VisuV_Geographie_Aegytpen_Deutschland.jpg)

# Projektbeschreibung

Visuv bietet einen einfachen, visuellen Vergleich zweier Länder der Welt und ihrer charakteristischen Kennzahlen.  
Stell dir vor, du liest einen Artikel über ein beliebiges Land in der Welt, sagen wir Myanmar.

Wo liegt das nochmals?  
Wie gross ist das denn eigentlich?  
Leben da auch viele Menschen?  
Sind die arm oder reich?  
Was macht das Land so wirtschaftlich gesehen?  
Wie ist das Klima da?  
Hat es viele Städte oder ist das eher ländlich geprägt? Was haben die denn für eine Regierung?

Um Informationen über das Ausland zu verstehen und einordnen zu können, muss man einen Bezug zu dem jeweiligen Land herstellen können. Diese Herausforderung geht VisuV an.

# Gesellschaftliche Herausforderung

Ich sehe die gesellschaftliche Herausforderung in Bildung, Information und Bewusstsein.  
Die Welt ist eins. Wir sollten unsere Nachbarn kennen und ein grobes Verständnis für deren spezifische Lebensumstände haben. Wir können nicht Bangladesh mit der Schweiz vergleichen, ohne zu berücksichtigen, wie gut es den einen geht und wie schlecht den anderen. Wie sehr die Realitäten des Lebensraums die Menschen prägt, kann man nur verstehen, wenn man diese Unterschiedlichkeit kennt und versteht.
Viele Menschen haben in Geografie in der Schule mehr oder weniger gut aufgepasst.

Wenn jetzt aber Bilder von einem gigantischen Containerschiff durch die Medien gehen, das direkt vor der Haustüre von arabischen Bäuerinnen parkt, dann wäre es gut, wenn die Aberwitzigkeit dieser Situation klar wird: ein Schiff kostet ein Wüstenland grosse Teile seiner Einnahmen. Wer sind die Menschen, die da leben? Wovon leben sie? Warum sind die Menschen in diesem Land so sehr auf diesen Kanal angewiesen?  
All das kann man besser beginnen zu verstehen, wenn man von diesem anderen Land mehr begreift.

# Technisches Themenfeld

Datenvisualisierung

# Technische Umsetzung

Die Umsetzung wird technisch eine RubyOnRails basierte WebApp sein und aus mehreren Teilen bestehen:

01 einer (Puffer-)SQL Datenbank der Kennzahlen der Länder aus denen die VIsualisierungen generiert werden  
02 mehreren Import-Skripten, die diese Daten aus anderen OpenData Quellen in die Datenbank synchronisieren (Ruby)  
03 einem Visualisierungsmodul, das aus den Daten jeweils für den Vergleich passende grafische SVG Daten generiert (Ruby generiert SVG)  
04 einem Web-Interface, um jeweils ein Widget zu konfigurieren und für den Export vorzubereiten (Journalisten wählen damit Land und relevanteste Daten aus für ihren jeweiligen Artikel) (Ruby generiert HTML/CSS/JS)  
05 einem Export-Modul, um das konfigurierte Widget für Seitenabrufe bereit zu machen (Ruby generiert HTML/CSS/JS)  
06 einem Export-Modul, um ausgewählte Vergleiche für Print Produkte vorzubereiten (Ruby generiert PDF/PNG)  

Der grösste Teil wird in Ruby programmiert, mit RubyOnRails vom Server aus bereitgestellt und digital mit HTML/CSS/JavaScript User Interface für Browser ausgeliefert, bzw. als PDF oder PNG Dateien exportiert.  

# Weitere Visualisierungsbeispiele

Aus einem Studierenden-Projekt der ZHdK zeigen [weitere Visualisierungsbeispiele](doc/examples/student_examples/index.md) auf, in welche Richtung das Projekt gehen wird.
